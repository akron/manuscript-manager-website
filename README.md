## Setup

Install Node:
> Goto [Node js website](http://nodejs.org/) and follow the instructions

Install git:
> Goto [Git website](http://git-scm.com/) and follow the instructions

Install bower and gulp:
> `npm install bower -g`

> `npm install gulp-cli -g`

Clone repository
> Goto FOLDER where you want to have the project

> `git clone https://akron@bitbucket.org/akron/manuscript-manager-website.git FOLDER_NAME`

Navigate to folder and run following

> `cd /c/projects/FOLDER_NAME`

> `npm install`

> `bower install`

> `gulp`


