  
var myApp = angular.module('myApp', [
  'ngRoute', 'mm.foundation'
  ]);

myApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
  $locationProvider.html5Mode(true);
  $routeProvider.
  when('/', {
    templateUrl: '/views/homeView.html',
  }).
  when('/about', {
    templateUrl: '/views/aboutView.html',
  }).
  when('/contact', {
    templateUrl: '/views/contactView.html',
  }).
  when('/facts', {
    templateUrl: '/views/factsView.html',
  }).
  when('/pricing', {  
    templateUrl: '/views/pricingView2.html',
  }).
  when('/tutorials', {  
    templateUrl: '/views/tutorialsView.html',
  }).
  when('/signup', {  
    templateUrl: '/views/signupView.html',
  }).
  otherwise({
    redirectTo: '404Error.html'
          //templateUrl: '404Error.html'
        });
}]);



