// -------------------------------------------------------------------------- //
/* Gulp Task Runner
 * -> $ npm install -g --save-dev gulp
 * -> $ npm install --save-dev gulp-sass
 */
 var gulp = require('gulp');
 //var clean = require('gulp-clean');
 //var uglify = require('gulp-uglify');
 var sass = require('gulp-sass');
 var plumber = require('gulp-plumber');
 var prefix = require('gulp-autoprefixer');
 var rename = require('gulp-rename');
 var inject = require('gulp-inject');
 var uncss = require('gulp-uncss');


//gulp.task('clean', function () {  
//  return gulp.src('build', {read: false})
//    .pipe(clean());
//});


// -> $ gulp sass
gulp.task('sass', function () {
  gulp.src('./sass/app.scss')
  .pipe(sass({ sourcemap: true, style: 'compact' }))
  .pipe(prefix('last 2 version', '> 1%', 'ie 8', 'ie 7'))
  .pipe(sass())
  .pipe(plumber())
  .pipe(gulp.dest('stylesheets'))

  gulp.src('./bower_components/foundation/scss/foundation.scss')
  .pipe(sass({ sourcemap: true, style: 'compact' }))
  .pipe(prefix('last 2 version', '> 1%', 'ie 8', 'ie 7'))
  .pipe(sass())
  .pipe(plumber())
  .pipe(gulp.dest('bower_components/foundation/css'))

});


//gulp.task('vendor', function() {  
//  return gulp.src('vendor/*.js')
//    .pipe(concat('vendor.js'))
//    .pipe(uglify())
//    .pipe(gulp.dest('build/vendor.js'))
//});

// Copy all static assets
gulp.task('copy', function() {
  gulp.src('bower_components/jquery/dist/jquery.js')
  .pipe(gulp.dest('javascript'));

  gulp.src('bower_components/angular/angular.min.js')
  .pipe(gulp.dest('javascript'));

  gulp.src('bower_components/angular/angular.min.js')
  .pipe(gulp.dest('javascript'));

  gulp.src('bower_components/angular/angular.min.js.map')
  .pipe(gulp.dest('javascript'));

  gulp.src('bower_components/angular-route/angular-route.min.js')
  .pipe(gulp.dest('javascript'));

  gulp.src('bower_components/foundation/js/vendor/modernizr.js')
  .pipe(gulp.dest('javascript'));
  
  gulp.src('bower_components/angular-route/angular-route.min.js.map')
  .pipe(gulp.dest('javascript'));

  gulp.src('bower_components/foundation/js/foundation.min.js')
  .pipe(gulp.dest('javascript'));

  gulp.src('bower_components/foundation/css/foundation.css')
  .pipe(gulp.dest('stylesheets'));
});

gulp.task('copy-index-html', function() {
  gulp.src('index.html')
      // Perform minification tasks, etc here
      .pipe(gulp.dest('/404Error.html'));
    });



//Copy all views to full html files to dest folder
gulp.task('inject-home', function() {
  gulp.src('./FullHtmlDemo.html')
  .pipe(inject(gulp.src(['./views/homeView.html']), {
    starttag: '<!-- inject:body:{{ext}} -->',
    transform: function (filePath, file) {
      return file.contents.toString('utf8')
    }
  }))
  .pipe(rename('home.html'))
  .pipe(gulp.dest('./dest'));
});

gulp.task('inject-about', function() {
  gulp.src('./FullHtmlDemo.html')
  .pipe(inject(gulp.src(['./views/aboutView.html']), {
    starttag: '<!-- inject:body:{{ext}} -->',
    transform: function (filePath, file) {
      return file.contents.toString('utf8')
    }
  }))
  .pipe(rename('about.html'))
  .pipe(gulp.dest('./dest'));
});

gulp.task('inject-contact', function() {
  gulp.src('./FullHtmlDemo.html')
  .pipe(inject(gulp.src(['./views/contactView.html']), {
    starttag: '<!-- inject:body:{{ext}} -->',
    transform: function (filePath, file) {
      return file.contents.toString('utf8')
    }
  }))
  .pipe(rename('contact.html'))
  .pipe(gulp.dest('./dest'));
});

gulp.task('inject-facts', function() {
  gulp.src('./FullHtmlDemo.html')
  .pipe(inject(gulp.src(['./views/factsView.html']), {
    starttag: '<!-- inject:body:{{ext}} -->',
    transform: function (filePath, file) {
      return file.contents.toString('utf8')
    }
  }))
  .pipe(rename('facts.html'))
  .pipe(gulp.dest('./dest'));
});

gulp.task('inject-pricing', function() {
  gulp.src('./FullHtmlDemo.html')
  .pipe(inject(gulp.src(['./views/pricingView.html']), {
    starttag: '<!-- inject:body:{{ext}} -->',
    transform: function (filePath, file) {
      return file.contents.toString('utf8')
    }
  }))
  .pipe(rename('pricing.html'))
  .pipe(gulp.dest('./dest'));
});

gulp.task('inject-signup', function() {
  gulp.src('./FullHtmlDemo.html')
  .pipe(inject(gulp.src(['./views/signupView.html']), {
    starttag: '<!-- inject:body:{{ext}} -->',
    transform: function (filePath, file) {
      return file.contents.toString('utf8')
    }
  }))
  .pipe(rename('signup.html'))
  .pipe(gulp.dest('./dest'));
});

gulp.task('inject-tutorials', function() {
  gulp.src('./FullHtmlDemo.html')
  .pipe(inject(gulp.src(['./views/tutorialsView.html']), {
    starttag: '<!-- inject:body:{{ext}} -->',
    transform: function (filePath, file) {
      return file.contents.toString('utf8')
    }
  }))
  .pipe(rename('tutorials.html'))
  .pipe(gulp.dest('./dest'));
});

//Copy css files to dest folder
gulp.task('copy-ccs-js-to-dest', function() {
  gulp.src('javascript/modernizr.js')
  .pipe(gulp.dest('dest/javascript'));

  gulp.src('javascript/foundation.min.js')
  .pipe(gulp.dest('dest/javascript'));

  gulp.src('javascript/jquery.js')
  .pipe(gulp.dest('dest/javascript'));

  gulp.src('stylesheets/foundation.css')
  .pipe(gulp.dest('dest/stylesheets'));Foundation.set_namespace = function() {};

  gulp.src('stylesheets/app.css')
  .pipe(gulp.dest('dest/stylesheets'));

  gulp.src('images/*')
  .pipe(gulp.dest('dest/images'));

//Uncss
//gulp.task('uncsss', function() {
  return gulp.src('./dest/stylesheets/app.css')
  .pipe(uncss({
    html: ['./dest/*.html']
  }))
  .pipe(gulp.dest('./dest/stylesheets/app.uncss.css'));

  return gulp.src('./dest/stylesheets/foundation.css')
  .pipe(uncss({
    html: ['./dest/*.html']
  }))
  .pipe(gulp.dest('./dest/stylesheets/foundation.uncss.css'));

//});


//Copy everything in dest folder to MANUSCRIPT MANAGER folder for andy
gulp.src('dest/**/*')
.pipe(gulp.dest('c:/Projects/peer-review-products/MANUSCRIPT MANAGER/martin_web'));


});

gulp.task('watch', function () {
  gulp.watch('./sass/*.scss', ['sass']);
});


// -> $ gulp
gulp.task('default', ['sass', 'copy', 'inject-home', 'inject-about', 'inject-contact', 'inject-facts', 'inject-pricing', 'inject-signup', 'inject-tutorials', 'copy-ccs-js-to-dest', 'copy-index-html']);